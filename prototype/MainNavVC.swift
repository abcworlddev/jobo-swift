//
//  MainNavVC.swift
//  jobo
//
//  Created by victory on 4/4/15.
//  Copyright (c) 2015 lordalexworks. All rights reserved.
//

import UIKit

class MainNavVC: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    
    override  func viewWillAppear(animated: Bool) {
        
        self.navigationBar.barTintColor = UIColor.clearColor()
        self.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.backgroundColor = UIColor.clearColor()
        self.navigationBar.translucent = true
        

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
